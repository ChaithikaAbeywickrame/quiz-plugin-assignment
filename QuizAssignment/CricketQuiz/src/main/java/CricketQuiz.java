
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.Timer;
//import java.util.Timer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This class is created to initailise a quiz called CricketQuiz implementing the Quiz interface
 * @author Chaithika Abeywickrame
 */
public class CricketQuiz implements Quiz{
    
    
    LinkedList<QuestionType> questions = new LinkedList<QuestionType>();
    Container pane1;
    int marks;
    QuizWindow qw;
    
    /**This Function is used to return the name of the  quiz used in loading the quiz through a plugin */
    @Override
    public String getName() {
        return "CricketQuiz";
        
    }

    /**This is used to create the questions with the preferred questiontypes using the pluginLoader 
     * and to add the questions to a linked list if it is not empty so if this method is recalled at the 
     * restart button in the GUI it won't be added again
     */
    @Override
    public void runQuiz(PluginLoader loader) {
        marks = 0;
        QuestionType s1 = loader.loadQuestion("ShortAnswerQT");
        QuestionType s2 = loader.loadQuestion("MultipleChoiceQT");
        QuestionType s3 = loader.loadQuestion("ShortAnswerQT");
        QuestionType s4 = loader.loadQuestion("MultipleChoiceQT");
        s1.makeShortAnswerQT("Who is the captain in Srilanka?", "Chandimal");
        s2.makeMultipleChoiceQT("Select the worlld cup winning captain", new String[]{"Dilshan","Arjuna","Mahela","Gayle"}, 1);
        s3.makeShortAnswerQT("When is the next world cup?","2019");
        s4.makeMultipleChoiceQT("Team recently visited SL",
                new String[] { "India", "Pakistan", "England", "Bangladesh" }, 2);
        if(questions.isEmpty()){
            questions.add(s1);
            questions.add(s2);
            questions.add(s3);
            questions.add(s4);
        }
        
    }  

    /** This is used to load each questiontype from the  array list
     * Also to pass the next question to be shown in the GUI
     */
    @Override
    public void loadWindow(Quiz myQuiz) {
        qw = new QuizWindow();
        this.pane1 = qw.setWindow(myQuiz);
        for(int i=0; i<questions.size(); i++){
            QuestionType q1 = questions.get(i);
            //used to get the next question
            QuestionType q2;
            if(i!= questions.size()-1){
                q2 = questions.get(i+1);
            }
            else{
                q2 = null;
            }
            //called the load pane function in the question type class to load the question.
            q1.loadPane(pane1,q2);  
        }
    }

    /**This method is created in order to loop through the questions and 
     * get the marks once the user finishs the quiz 
     */
    @Override
    public void viewMarks(){
        marks = 0;
        for(int i=0; i<questions.size(); i++){
            QuestionType q1 = questions.get(i);
            marks = marks + q1.getMark();
        }
        int total = questions.size();
        JOptionPane.showMessageDialog(null,"You have Scored "+ marks +" out of "+ total);
        qw.window.dispose();
        mainGUI mg = new mainGUI();
        mg.setVisible(true);
    }
}
