
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This class is created to design a short answer question type
 * This is  used to get a answer as a user input for a given question
 * @author Chaithika Abeywickrame
 */
public class ShortAnswerQT extends QuestionType {
    
    //Variables assigned to store the values in the class
    private String question;
    private String answer;
    private int mark;
    
    //function to return the questionType name 
    public String getName() {
        return "ShortAnswerQT";
    }

    //function to make short answer where the passed variables are assigned to class variables
    @Override
    public void makeShortAnswerQT(String question, String answer) {
        this.question = question;
        this.answer = answer;
        
    }    

    //function to set the contents in the pane 
    //referred at the GUIDemo.java published at blackboard
    @Override
    public void loadPane(Container pane, QuestionType q1) {
        JPanel dynamicPanel = new JPanel() {
            @Override
            public Dimension getMaximumSize() {
                return new Dimension(super.getMaximumSize().width, super.getPreferredSize().height);
            }
        };

        dynamicPanel.setLayout(new GridLayout(0, 4));

        // Purely aesthetic.
        dynamicPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        dynamicPanel.setBorder(new CompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED),
                BorderFactory.createEmptyBorder(8, 8, 8, 8)));
        pane.add(dynamicPanel);
        
        JLabel label = new JLabel(question);
        final JTextField textField = new JTextField();
        JButton showBtn = new JButton("Submit");
        //next question set to the next button to be displayed as a tooltip
        JButton nxtBtn = new JButton("Next");
        if(q1 != null){
            nxtBtn.setToolTipText(q1.getQuestion());
        }else{
            nxtBtn.setToolTipText("End of Quiz");
        }
        
        //submit button functionality has been added here to 
        //get the answer compared and to set the mark
        showBtn.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                String input = textField.getText();
                if(input.equals(answer)){
                    showBtn.setEnabled(false);
                    mark = 1;
                }
                else{
                    showBtn.setEnabled(false);
                    mark =0;
                }
            }

        });

        dynamicPanel.add(label);
        dynamicPanel.add(textField);
        dynamicPanel.add(showBtn);
        dynamicPanel.add(nxtBtn);
        
    }

    //function created to return the mark for a question
    @Override 
    int getMark(){
        return mark;
    }
    
    //function to return the  question variable in the class 
    @Override
    String getQuestion(){
        return question;
    }
}
