Way to run the Application
	1. Open the Quiz Assignment
	2. Open command prompt in the Quiz Assignment directory
	3. Enter command 'gradle build'
	4. Then once the build is successful enter command 'java -cp "JarFiles/*" mainGUI'
	5. Enter the name of a quiz by the available list




Available Quizzes in the  system:
	"CricketQuiz"

Available Question Types in the system:
	"ShortAnswerQT"
	"MultipleChoiceQT"

Way to initailize question type:
	"ShortAnswerQT" 	: 'QuestionType s1 = loader.loadQuestion("ShortAnswerQT");'
			  's1.makeShortAnswerQT("Who is the captain in Srilanka?", "Chandimal");'

	"MultipleChoiceQT"	: 'QuestionType s2 = loader.loadQuestion("MultipleChoiceQT");'	
			  's2.makeMultipleChoiceQT("Select the worlld cup winning captain", new String[]{"Dilshan","Arjuna","Mahela","Gayle"}, 1);'