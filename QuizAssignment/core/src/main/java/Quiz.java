
import java.awt.Container;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Interface created to generate quizes implementing this interface
 * @author Chaithika Abeywickrame
 */
public interface Quiz {
    //function to return the name
    String getName();
    //function to rn the quiz and a PluginLoader  object has been passed to create relevant classes within the class
    void runQuiz(PluginLoader loader);
    //Function used to load the quiz window
    void loadWindow(Quiz name);
    //function to view the marks
    void viewMarks();
}
