
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This is the primary window where for each quiz the question types will be loaded into.
 * @author Chaithika Abeywickrame
 */
public class QuizWindow {
    final JFrame window = new JFrame("Quiz Window!!");
    Container pane;
    JPanel dynamicPanel;
    int marks;
   
    //This  sets the window  along with a toolbar with buttons to restart quiz and to finish quiz
    //This was referred using the GUIDemo.java file published on black board
    //This method is  created in order to return a Container which would be accessed at the quizes
    public Container setWindow(Quiz qName){
        window.setSize(972, 577);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(true);
        pane = window.getContentPane();
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
        JToolBar toolbar = new JToolBar();
        toolbar.setAlignmentX(Component.LEFT_ALIGNMENT);
        JButton finishQuiz = new JButton("Complete Quiz");
        /*functioning of the finish quiz button is initialised*/
        finishQuiz.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null,
                        "Would you like to finish quiz?", "Warning", dialogButton);
                if(dialogResult == 0){
                    qName.viewMarks();
                }
            }
            
        });

        /**functionality of the restart quiz button is initialised */
        JButton restartQuiz = new JButton("Restart Quiz");
        restartQuiz.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Would you like to Restart quiz?", "Warning",
                        dialogButton);
                if (dialogResult == 0) {
                    window.dispose();
                    qName.loadWindow(qName);
                }
            }
        });
        toolbar.add(finishQuiz);
        toolbar.add(restartQuiz);
        pane.add(toolbar);
        window.setVisible(true);
        window.setLocationRelativeTo(null);
        return pane;
    } 
    
}
