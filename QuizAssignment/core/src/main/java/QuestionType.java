
import java.awt.Container;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Class created in order to to aid the creation of question types extending the methods in this class
 * @author Chaithika Abeywickrame
 */
public class QuestionType {
    //method to return the name in the extended classes
    String getName(){
        return "name";
    };
    //method to return the  
    String getQuestion(){
        return "questionname";
    }
    //function used to load a short answer question
    void makeShortAnswerQT(String question, String answer){};
    //function used to load a multiple choice question
    void makeMultipleChoiceQT(String question, String[] answers, int index){};
    //function used to assign a questiontype to a dynamic panel
    void loadPane(Container pane, QuestionType q1){};
    //function used to get the mark for each question type
    int getMark(){
        return 1;
    }
    
}
