
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This Class is to load the relevant class for a given name Reference made at
 * Reference made at the Assgnment Tips pdf document published on blackboard
 * "https://lms.curtin.edu.au/bbcswebdav/pid-6083485-dt-content-rid-32209431_1/courses/2018_2_COMP3003_V1_L73_A1_INT_642629/SEC_2018s2_AssignmentTips.pdf"
 * 
 * @author Chaithika Abeywickrame
 */
public class PluginLoader extends ClassLoader {
    
    //Function to load the Quiz using its name
    public Quiz loadPlugin(String quizName){
        try{
            Class<?> cls = Class.forName(quizName);
            return (Quiz) cls.newInstance();    
        }catch(NullPointerException e1){
             System.out.println("Null Pointer Exception Caught");
        }catch(InstantiationException e){
             System.out.println("Instantiation Exception Caught");
        }catch(IllegalAccessException e){
             System.out.println("Illegal Access Exception Caught");
        }catch(ClassNotFoundException e){
            System.out.println("Unable to locate or find Class");
        }
        return null;
    }
    
    //Function created to load a questionType from a given name
    public QuestionType loadQuestion(String qName){       
        try {
            Class<?> clsQ = Class.forName(qName);
            return (QuestionType) clsQ.newInstance();
        } catch (IllegalAccessException ex) {
            System.out.println("Illegal Access Exception");
        }catch(InstantiationException e1){
            System.out.println("Instantiation Exception has been caught!");
        }catch(ClassNotFoundException e){
            System.out.println("Unable to locate or find Class");
        }
        return null;
    }
}
