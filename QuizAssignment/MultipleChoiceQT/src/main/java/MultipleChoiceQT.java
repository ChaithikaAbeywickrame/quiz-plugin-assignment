
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This is a multiple choice question type created by extending the Questiontype class
 * This is used to get a answer for a question choosing from a given set of answers
 * @author Chaithika Abeywickrame
 */
public class MultipleChoiceQT extends QuestionType{
    
    //variables created to hold the data in the class
    private String q;
    private String[] ans;
    private int a;
    int mark;

    /**This function is used to return the name of the class */
    @Override
    public String getName() {
        return "MultipleChoiceQT";
    }

    /**Function used to create the multiple choice question
     * this accepts the question, answers array and the index of the answer
     */
    @Override
    public void makeMultipleChoiceQT(String question, String[] answers, int answer) {
        q = question;
        a = answer;
        ans = new String[answers.length];
 
        for (int j=0; j<answers.length; j++){
            ans[j] = answers[j];
        }
    }

    /**This function accepts the pane returned from them main window
     * and a qestionType is passed to display the next question from the button
     * the UI designed is referred by the GUIDemo.java file published in courseweb 
     */
    @Override
    public void loadPane(Container pane, QuestionType q1) {
        JPanel dynamicPanel = new JPanel() {
            @Override
            public Dimension getMaximumSize() {
                return new Dimension(super.getMaximumSize().width, super.getPreferredSize().height);
            }
        };

        dynamicPanel.setLayout(new GridLayout(0, 4));

        // Purely aesthetic.
        dynamicPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        dynamicPanel.setBorder(new CompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED),
                BorderFactory.createEmptyBorder(8, 8, 8, 8)));
        pane.add(dynamicPanel);
        
        JLabel label = new JLabel(q);
        JPanel radioButtonPanel = new JPanel();
        radioButtonPanel.setLayout(new BoxLayout(radioButtonPanel, BoxLayout.Y_AXIS));
        
        //assign the answers to the radio buttons
        JRadioButton[] radioButtons = new JRadioButton[ans.length];
        for (int a=0; a< ans.length; a++){
            radioButtons[a] = new JRadioButton(ans[a]);
        }
        ButtonGroup group = new ButtonGroup();
        for(int i = 0; i < radioButtons.length; i++)
        {
            // Add each button to the GUI.
            radioButtonPanel.add(radioButtons[i]);
                    
            // Ensure only one radio button can be selected at a time.
            group.add(radioButtons[i]);
        }
                
        JButton showBtn = new JButton("Submit");
        JButton nxtBtn = new JButton("Next");
        //used to display the next question in the next button as a tooltip
        if(q1 != null){
            nxtBtn.setToolTipText(q1.getQuestion());
        }else{
            nxtBtn.setToolTipText("End of Quiz");
        }
        
        //used to get the answer and assign a mark for the selected answer
        showBtn.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                for(int i = 0; i < radioButtons.length; i++)
                {
                    if(radioButtons[i].isSelected())
                    {
                        if(i == a){
                            showBtn.setEnabled(false);
                            mark = 1;
                        }
                        else{
                            showBtn.setEnabled(false);
                            mark = 0;
                        }
                    }
                }
            }
        
        });
        
        dynamicPanel.add(label);
        dynamicPanel.add(radioButtonPanel);
        dynamicPanel.add(showBtn);
        dynamicPanel.add(nxtBtn);
    }
    
    //function to return the mark
    @Override 
    int getMark(){
        return mark;
    }

    //function to get the function
    @Override
    String getQuestion(){
        return q;
    }
}
